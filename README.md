# Summary

Deploy upgrades to `minecraft.kurtbomya.com` automatically

# How it works

- Leveraging GitLab CI to start an environment
- **Pull the latest** `BuildTools.jar` from spigot
- **Build** the new jar in the CI container
- Register the jar as **an artifact**
- Push the jar to kurtbomya.com using **deploy keys**